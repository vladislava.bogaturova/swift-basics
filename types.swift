//Типы данных. Язык со строгой типизацией

//Int -целое число
var number: Int = 10
var a = 42
var b = -27

Int.min
Int.max

UInt.min
UInt.max

//--------

Int8.min
Int8.max

UInt8.min
UInt8.max

//--------

Int16.min
Int16.max

UInt16.min
UInt16.max

//--------

Int32.min
Int32.max

UInt32.min
UInt32.max

//--------

Int64.min
Int64.max

UInt64.min
UInt64.max

// Double(max 15 char), Float(max 6 char after coma)
var x = 5.47
var c = -300.145
var float: Float = 1.45

//String

var greet = "Hello, world"
var t ="Hello"
var u = "swift"
var sum = t+u

pront("Hello \(u)") // Интерполяция


//Bool
var areYouHappy = true


if areYouHappy {
    print("Good")
} else {
    print("Bad")
}



//Мы не можем складывать переменнные с разным типом данных, будет ошибка. Но мы можем привести один тип данных к другому

var sum = Int(c) + a //Int не округляет число, он убирает все что после точки
var sum = c + Double(a)