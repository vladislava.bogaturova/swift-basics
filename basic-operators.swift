// Базовые операторы

// Унарные: префиксные -a, !b  постфиксные с!
// Бинарные: 1 + 2
// Тернарные: a ? b : c


// Оператор присваивания =

let a = 12
var b = 5
b = a 

// арифметические операторы +, -, *, /, %

let x = 12
let y = 4

x + y
x - y
x * y
x / y

let h = "Hello, "
let g ="swift"

h + g

x % y
x % -y // будет то же самое

// 12/ (5 * 2) + 2

// Составные операторы присваивания +=, -=

var c = 1
c += 2 // 3 c = c+2


var count = 0
count +=1

// Операторы сравнения ==, !=, >, <, >=, <=

let t = 15
let p = 10 
t == p // false
t != p // true
t > p
t < p
t >= p
t <= p

let name = " world"

if name == "world"{
    print "hello \(name)"
} else {
    print ("Something went wrong")
}


// Тернарный условный оператор
// выражение ? действие 1 : действие 2

let firstCard = 11
let secondCard = 10

firstCard == secondCard ? print("Cards are the same") : print("Cards sre different")

// Оператор замкнутого/закрытого диапазона (a...b), оператор полузамкнутого/полузакрытого диапазона (a..<b)

// Логические операторы !a, &&, ||

let areYouHappy = true
let isTheWeatherGood = true

if areYouHappy && isTheWeatherGood {
    print("Go outside")
} else {
    print ("Stay home")
}
firstCard == secondCard ? print("Cards are the same"):print("Cards sre different")

// Оператор замкнутого/закрытого диапазона (a...b), оператор полузамкнутого/полузакрытого диапазона (a..<b)

// Логические операторы !a, &&, ||
